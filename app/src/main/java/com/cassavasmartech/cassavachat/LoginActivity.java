package com.cassavasmartech.cassavachat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity implements View.OnLongClickListener, View.OnClickListener {
    private String TAG = LoginActivity.class.getSimpleName();
    private EditText email;
    private EditText password;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Log.i(TAG, "We have entered onCreate");
        Log.e(TAG, "Something bad happened");

        email = findViewById(R.id.email);
        password = findViewById(R.id.editTextTextPassword);
        Button login = findViewById(R.id.button);

        if (savedInstanceState!=null) {
            count = savedInstanceState.getInt("count", 0);
        }

        login.setOnClickListener(this);
        login.setOnLongClickListener(this);
        findViewById(R.id.termsAndConditions).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(view->{
            count++;
            Toast.makeText(this, "Count = "+count, Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", count);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id==R.id.termsAndConditions) {
            Uri uri = Uri.parse("http://www.google.com");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id==R.id.button) {
            String userEmail = email.getText().toString();
            String userPassword = password.getText().toString();
            if (AuthHelper.checkCredentials(userEmail, userPassword)) {
                //User details correct. Move on to MainActivity
                Intent intent = new Intent(getApplicationContext(), ChatListActivity.class);
                startActivity(intent);
                //Remove Login activity from back-stack when login is success
                finish();
            } else {
                //Wrong Credentials entered
                Toast.makeText(getApplicationContext(),
                        "Login failed. Please check your details", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onLongClick(View view) {
        Toast.makeText(getApplicationContext(), "Long click", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    protected void onPause() {
        Toast.makeText(getApplicationContext(), "onPause Callback", Toast.LENGTH_SHORT).show();
        super.onPause();
    }
}