package com.cassavasmartech.cassavachat;

import com.cassavasmartech.cassavachat.models.User;

public class BuilderPatternDemo {

    public static void main(String[] args) {
        User user = new User.Builder(12L, "a@a.com")
                .address("20 Borrowdale road")
                .phoneNumber("07777777")
                .build();
    }

}
