package com.cassavasmartech.cassavachat;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cassavasmartech.cassavachat.models.Chat;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.List;

public class ChatListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupChatList();
    }

    private void setupChatList() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ChatAdapter adapter = new ChatAdapter(getChatList());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private List<Chat> getChatList() {
        //Create A dummy list of chat object
        return Arrays.asList(
                new Chat("Bob Franks", "0777732342"),
                new Chat("Elizabeth Moyo", "0999737243"),
                new Chat("Tino Marere", "0734526916"),
                new Chat("Kodi", "08722938372")
        );
    }

    public void onFabClicked(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", v-> Toast.makeText(this, "From SnackBar action", Toast.LENGTH_SHORT).show()).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate our menu into the activity window
        getMenuInflater().inflate(R.menu.menu_chat_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Extract the ID of the item that was clicked
        int id = item.getItemId();
        if(id == R.id.action_settings) {
            //Open SettingsActivity
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        } else if (id == R.id.action_about) {
            showAboutDialog();
        } else if (id == R.id.action_favorites) {
            //TODO Add Implementation
        }
        return true;
    }

    private void showAboutDialog() {
        //Build and show a dialog using AlertDialog Builder
        new AlertDialog.Builder(this)
                .setTitle("About Cassava Chat")
                .setIcon(R.drawable.ic_chat)
                .setMessage("Cassava Chat is a content rich app made to connect people all across" +
                " Africa.\n\nCopyright Cassava Smartech 2021\n\nAll rights reserved")
                .setPositiveButton("Okay", (dialogInterface, i) -> Toast.makeText(ChatListActivity.this, "Dialog dismissed", Toast.LENGTH_SHORT).show())
                .setCancelable(false)
                .show();
    }

}